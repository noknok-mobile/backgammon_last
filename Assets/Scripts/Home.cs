﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Home : MonoBehaviour
{
    public int value;
    public GameObject indicator;
    public List<Checker> checkers;
    public BoxCollider myCollider;
    public List<Cell> cellsAtHome = new List<Cell>();
    public Vector3 checkerPositionAtHome;

    private void Start()
    {
        myCollider = GetComponent<BoxCollider>();
    }
    public Checker ReturnLastChecker()
    {
        return checkers.Count != 0 ? checkers.Last() : null;
    }

    public bool CheckEnabledCell(Checker.ColorChecker colorChecker)
    {
        int numberOfCheckersAtHome = 0;
        foreach (Cell cell in cellsAtHome)
        {
            if(cell.checkers.Count > 0 && cell.checkers.Last().colorChecker == colorChecker)
            {
                numberOfCheckersAtHome += cell.checkers.Count;
            }
        }
        numberOfCheckersAtHome += checkers.Count;
        if(numberOfCheckersAtHome == 14 || numberOfCheckersAtHome == 15)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void EnabledCell()
    {
        indicator.SetActive(true);
        myCollider.enabled = true;
        gameObject.layer = 9;
    }

    public void DisabledCell()
    {
        indicator.SetActive(false);
        myCollider.enabled = false;
        gameObject.layer = 2;
    }

    public bool CheckerEnterToHome(Checker checker)
    {
        checkers.Add(checker);
        if(checkers.Count == 15)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Vector3 CheckerPositionAtHome()
    {
        return checkerPositionAtHome;
    }
}
