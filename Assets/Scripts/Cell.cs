﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public int value;
    public GameObject indicator;

    public List<Checker> checkers = new List<Checker>();    
    private BoxCollider _myCollider;

    private void Start()
    {
        _myCollider = GetComponent<BoxCollider>();
    }

    public Checker ReturnAnAvailableChecker()
    {
        Checker checker = checkers.Last();
        return checker;
    }

    public void TookChecker(Checker checker)
    {
        checkers.Remove(checker);
        MoveAllCheckersAfterCapture();
    }

    public Vector3 NewCheckerPosition()
    {
        Vector3 newPosition = Vector3.zero;
        if (checkers.Count == 0)
        {
            newPosition = transform.position;
        }
        else if (checkers.Count < 3)
        {
            newPosition = checkers.Last().transform.position + transform.forward * 0.3f;
        }
        else
        {
            newPosition = MoveAllCheckerBeforeAdding();
        }
        newPosition.y = -0.13f;
        return newPosition;
    }

    private Vector3 MoveAllCheckerBeforeAdding()
    {
        float distance = (0.0166666666666667f - 0.2f / checkers.Count);
        if (value < 13)
        {
            for (int i = 0; i < checkers.Count; i++)
            {
                Vector3 newPos = checkers[i].transform.position + new Vector3(0, 0, distance * i);
                checkers[i].ChangePosition(newPos, true);
            }
            Vector3 newPosition = checkers.Last().transform.position;
            newPosition.z += Vector3.Distance(checkers[0].transform.position, checkers[1].transform.position);
            return newPosition;
        }
        else
        {
            for (int i = 0; i < checkers.Count; i++)
            {
                Vector3 newPos = checkers[i].transform.position + new Vector3(0, 0, -distance * i);
                checkers[i].ChangePosition(newPos, true);
            }
            Vector3 newPosition = checkers.Last().transform.position;
            newPosition.z += Vector3.Distance(checkers[0].transform.position, checkers[1].transform.position) * -1;
            return newPosition;
        }
    }

    private void MoveAllCheckersAfterCapture()
    {
        float distance = (0.2f / checkers.Count - 0.0166666666666667f);        
        if (checkers.Count > 2)
        {
            if (value < 13)
            {
                for (int i = 0; i < checkers.Count; i++)
                {
                    Vector3 newPos = checkers[i].transform.position + new Vector3(0, 0, distance * i);
                    checkers[i].ChangePosition(newPos, true);
                }
            }
            else
            {
                for (int i = 0; i < checkers.Count; i++)
                {
                    Vector3 newPos = checkers[i].transform.position + new Vector3(0, 0, -distance * i);
                    checkers[i].ChangePosition(newPos, true);
                }

            }
        }
    }

    public void AddNewChecker(Checker checker)
    {
        checkers.Add(checker);
    }

    public Checker TakeAnAvailableCheckerForVerification()
    {
        if (checkers.Count > 0)
        {
            Checker checker = checkers.Last();
            return checker;
        }
        else
        {
            return null;
        }
    }

    public void EnabledCell()
    {
        _myCollider.enabled = true;
        indicator.SetActive(true);
        gameObject.layer = 9;
    }

    public void DisabledCell()
    {
        _myCollider.enabled = false;
        indicator.SetActive(false);
        gameObject.layer = 2;
    }
}

