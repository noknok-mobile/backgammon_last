﻿using System;

public interface INetworkable
{
    event Action<NetworkDataType, NetworkData> OnReceiveData;

    void ConnectToServer();
    void CreateRoom();
    void JoinToRoom();
    void SendData(NetworkDataType commandType, NetworkData data);
    void ReceiveData(NetworkDataType commandType, NetworkData data);
    bool IsPlayerMine();
    bool IsFirstPlayerSet();
    bool IsPlayerCanThrow();
}