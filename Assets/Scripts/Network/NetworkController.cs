﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkController : MonoBehaviour
{
    public static NetworkController Instance;

    private INetworkable _network;
    private bool _isChange;

    void Awake()
    {
        SetSingleton();
        //Connect();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            if (_isChange)
            {
                Screen.SetResolution(960, 540, false);
            }
            else
            {
                Screen.SetResolution(540, 960, false);
            }
            _isChange = !_isChange;
        }
    }

    public bool IsFirstPlayerSet()
    {
        return _network.IsFirstPlayerSet();
    }

    public bool IsPlayerMine()
    {
        return _network.IsPlayerMine();
    }

    public void SendData(NetworkDataType type, NetworkData data)
    {
        _network.SendData(type, data);
    }

    public void ReceiveData(NetworkDataType type, NetworkData data)
    {
        _network.ReceiveData(type, data);
    }

    public INetworkable GetNetwork()
    {
        return _network;
    }

    public bool IsPlayerCanThrow()
    {
        return _network.IsPlayerCanThrow();
    }

    public void Connect()
    {
        _network = GetComponent<INetworkable>();
        if (_network == null) return;

        _network.ConnectToServer();
    }

    private void SetSingleton()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }
}
