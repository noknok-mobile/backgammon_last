﻿using UnityEngine;

public class NetworkData
{
    public string ObjectID;
    public Vector3 Position;
    public Vector3 Rotation;
    public int ThrowDiceResult;
    public NetworkDataType Type;
    public Player.PlayerColor PlayerColor;
    public int[] CheckerValues;
    public bool IsNeedChangeFastPosition;
    public int CellIndex;
    public float OutlineDiceStrong;
    public string CellID;
    public float Lag;
}