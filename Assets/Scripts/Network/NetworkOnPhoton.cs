﻿using Photon;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkOnPhoton : PunBehaviour, INetworkable
{
    private const string GAME_VERSION = "1";
    private const int MAX_PLAYERS_IN_ROOM = 2;

    [SerializeField] private GameObject _photonHandlerPrefab;

    private bool _isConnecting;
    private PhotonViewHandler _photonViewHandler;

    public event Action<NetworkDataType, NetworkData> OnReceiveData;

    private void OnLevelWasLoaded(int level)
    {
        if (level == 2)
        {
            _photonViewHandler = PhotonNetwork.Instantiate(_photonHandlerPrefab.name, Vector3.zero, Quaternion.identity, 0).GetComponent<PhotonViewHandler>();
        }
    }

    public void ConnectToServer()
    {
        _isConnecting = true;
        PhotonNetwork.automaticallySyncScene = true;
        if(PhotonNetwork.connecting)
        {
            JoinToRoom();
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings(GAME_VERSION);
        }
    }

    public void CreateRoom()
    {
        PhotonNetwork.playerName = "Player " + "White";
        string roomName = "test" + UnityEngine.Random.Range(0, 500);
        PhotonNetwork.CreateRoom(roomName, new RoomOptions() { MaxPlayers = MAX_PLAYERS_IN_ROOM, EmptyRoomTtl = 1 }, null);
    }

    public void JoinToRoom()
    {
        PhotonNetwork.playerName = "Player " + "Black";
        PhotonNetwork.JoinRandomRoom();
    }

    public void SendData(NetworkDataType commandType, NetworkData data)
    {
        _photonViewHandler.SendData(commandType, data);
    }

    public void ReceiveData(NetworkDataType commandType, NetworkData data)
    {
        OnReceiveData?.Invoke(commandType, data);
    }

    public bool IsPlayerMine()
    {
        return _photonViewHandler.View.isMine;
    }

    public bool IsFirstPlayerSet()
    {
        return PhotonNetwork.isMasterClient;
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Region: " + PhotonNetwork.networkingPeer.CloudRegion);
        if (_isConnecting)
        {
            Debug.Log("OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room.\n Calling: PhotonNetwork.JoinRandomRoom(); Operation will fail if no room found");
            JoinToRoom();
        }
    }

	public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
	{
		Debug.Log("OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 2}, null);");
        CreateRoom();
    }

	public override void OnDisconnectedFromPhoton()
	{
        SceneManager.LoadScene("MainMenu");
		Debug.LogError("Disconnected");
		_isConnecting = false;
	}

	public override void OnJoinedRoom()
	{
		Debug.Log("OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running. For reference, all callbacks are listed in enum: PhotonNetworkingMessage");
		if (PhotonNetwork.room.PlayerCount == 1)
		{
			Debug.Log("We load the scene");
			PhotonNetwork.LoadLevel("GameScene");
		}
	}

    public bool IsPlayerCanThrow()
    {
        if(PhotonNetwork.room.PlayerCount == 2)
        {
            return true;
        }
        return false;
    }
}