﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class PhotonViewHandler : Photon.MonoBehaviour, IPunObservable
{
    public PhotonView View { get; private set; }

    private NetworkDataType _currentType;
    private NetworkData _currentData;

    private Queue<NetworkDataType> _typeQueue = new Queue<NetworkDataType>();
    private Queue<NetworkData> _dataQueue = new Queue<NetworkData>();
    private int index;

    void Awake()
    {
        View = photonView;
    }

    void Start()
    {
        View = photonView;
    }

    public void SendData(NetworkDataType type, NetworkData data)
    {
        if(type != NetworkDataType.ThrowDice && type != NetworkDataType.ChangeCheckerPosition)
        {
            CallRPC(type, data);
            return;
        }
        _typeQueue.Enqueue(type);
        _dataQueue.Enqueue(data);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            WriteData(stream);
        }
        else
        {
            ReadData(stream, info);
        }
    }

    private void WriteData(PhotonStream stream)
    {
        index = _typeQueue.Count;
        stream.Serialize(ref index);
        while (_typeQueue.Count > 0)
        {
            _currentType = _typeQueue.Dequeue();
            _currentData = _dataQueue.Dequeue();
            stream.SendNext(_currentType);
            switch (_currentType)
            {
                case NetworkDataType.ThrowDice:
                    stream.SendNext(_currentData.Position);
                    stream.SendNext(_currentData.Rotation);
                    stream.SendNext(_currentData.PlayerColor);
                    stream.SendNext(_currentData.ObjectID);
                    break;
                case NetworkDataType.ChangeCheckerPosition:
                    stream.SendNext(_currentData.Position);
                    stream.SendNext(_currentData.ObjectID);
                    stream.SendNext(_currentData.IsNeedChangeFastPosition);
                    break;
            }
        }
    }

    private void ReadData(PhotonStream stream, PhotonMessageInfo info)
    {
        stream.Serialize(ref index);
        for (int i = 0; i < index; i++)
        {
            _currentData = new NetworkData();
            int type = (int)stream.ReceiveNext();
            _typeQueue.Enqueue((NetworkDataType)type);

            switch ((NetworkDataType)type)
            {
                case NetworkDataType.ThrowDice:
                    _currentData.Position = (Vector3)stream.ReceiveNext();
                    _currentData.Rotation = (Vector3)stream.ReceiveNext();
                    _currentData.PlayerColor = (Player.PlayerColor)stream.ReceiveNext();
                    _currentData.ObjectID = (string)stream.ReceiveNext();
                    _dataQueue.Enqueue(_currentData);
                    break;
                case NetworkDataType.ChangeCheckerPosition:
                    _currentData.Position = (Vector3)stream.ReceiveNext();
                    _currentData.ObjectID = (string)stream.ReceiveNext();
                    _currentData.IsNeedChangeFastPosition = (bool)stream.ReceiveNext();
                    _currentData.Lag = Mathf.Abs((float)(PhotonNetwork.time - info.timestamp));
                    _dataQueue.Enqueue(_currentData);
                    break;
            }
        }
        
        while(_typeQueue.Count > 0)
        {
            NetworkController.Instance.ReceiveData(_typeQueue.Dequeue(), _dataQueue.Dequeue());
        }
    }

    private void CallRPC(NetworkDataType type, NetworkData data)
    {
        switch(type)
        {
            case NetworkDataType.AddCellToChecker:
                View.RPC("OnAddCellToCheckerRPCCall", PhotonTargets.Others, data.ObjectID, data.CellID, type);
                break;
            case NetworkDataType.GameOver:
                View.RPC("OnGameOver", PhotonTargets.Others, data.PlayerColor, type);
                break;
            case NetworkDataType.FirstThrowResult:
                View.RPC("OnGetFirstThrowResult", PhotonTargets.Others, data.PlayerColor, data.ThrowDiceResult, type);
                break;
            case NetworkDataType.ChangePlayer:
                View.RPC("OnChangePlayer", PhotonTargets.Others, data.PlayerColor, type);
                break;
            case NetworkDataType.ThrowResult:
                View.RPC("OnThrowResult", PhotonTargets.Others, data.ThrowDiceResult, type);
                break;
            case NetworkDataType.TookChecker:
                View.RPC("OnTookChecker", PhotonTargets.Others, data.Position, data.ObjectID, data.CheckerValues, data.CellIndex, type);
                break;
            case NetworkDataType.UnselectChecker:
                View.RPC("OnUnselectChecker", PhotonTargets.Others, data.ObjectID, type);
                break;
            case NetworkDataType.ChangeDiceOutline:
                View.RPC("OnChangeDiceOutline", PhotonTargets.Others, data.ObjectID, data.OutlineDiceStrong, type);
                break;
            case NetworkDataType.GoHome:
                View.RPC("OnGoHome", PhotonTargets.Others, data.ObjectID, data.Position, data.Rotation, data.PlayerColor, type);
                break;
        }
    }

    #region PUN_RPC
    [PunRPC]
    private void OnAddCellToCheckerRPCCall(string objectID, string cellID, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = objectID;
        data.CellID = cellID;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnGameOver(Player.PlayerColor color, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.PlayerColor = color;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnGetFirstThrowResult(Player.PlayerColor color, int diceResult, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ThrowDiceResult = diceResult;
        data.PlayerColor = color;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnChangePlayer(Player.PlayerColor color, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.PlayerColor = color;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnThrowResult(int throwDiceResult, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ThrowDiceResult = throwDiceResult;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnTookChecker(Vector3 pos, string objectID, int[] checkerValues, int cellIndex, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.Position = pos;
        data.ObjectID = objectID;
        data.CheckerValues = checkerValues;
        data.CellIndex = cellIndex;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnUnselectChecker(string objectID, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = objectID;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnChangeDiceOutline(string objectID, float outlineDiceStrong, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = objectID;
        data.OutlineDiceStrong = outlineDiceStrong;
        NetworkController.Instance.ReceiveData(type, data);
    }

    [PunRPC]
    private void OnGoHome(string objectID, Vector3 position, Vector3 rotation, Player.PlayerColor color, NetworkDataType type)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = objectID;
        data.Position = position;
        data.Rotation = rotation;
        data.PlayerColor = color;
        NetworkController.Instance.ReceiveData(type, data);
    }
    #endregion
}