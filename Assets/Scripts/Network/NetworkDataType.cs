﻿public enum NetworkDataType
{
    ChangePlayer,
    ThrowDice,
    ThrowResult,
    FirstThrowResult,
    TookChecker,
    UnselectChecker,
    ChangeCheckerPosition,
    ChangeDiceOutline,
    GameOver,
    GoHome,
    AddCellToChecker
}