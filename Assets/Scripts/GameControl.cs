﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    public enum GameProgress
    {
        FirstThrow,
        PlayerWhite,
        PlayerBlack
    }

    public GameProgress gameProgress;
    public Player playerWhile;
    public Player playerBlack;
    private int firstThrowResultWhileDice;
    private int firstThrowResultBlackDice;

    private void Start()
    {
        FirstThrow();
    }

    private void OnEnable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData += ReceiveData;
    }

    private void OnDisable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData -= ReceiveData;
    }

    private void FirstThrow()
    {
        playerWhile.PrepareADieBeforeTheFirstThrow();
        playerBlack.PrepareADieBeforeTheFirstThrow();
    }

    public void GetResultFirstThrow(Player.PlayerColor playerColor, int value)
    {
        var data = new NetworkData();
        data.ThrowDiceResult = value;
        data.PlayerColor = playerColor;
        NetworkController.Instance.SendData(NetworkDataType.FirstThrowResult, data);
        switch (playerColor)
        {
            case Player.PlayerColor.While:
                firstThrowResultWhileDice = value;
                break;

            case Player.PlayerColor.Black:
                firstThrowResultBlackDice = value;
                break;
        }
        if (firstThrowResultWhileDice != 0 && firstThrowResultBlackDice != 0)
        {
            CountingTheResultsOfTheFirstThrow();
        }
    }

    private void CountingTheResultsOfTheFirstThrow()
    {
        if (firstThrowResultWhileDice > firstThrowResultBlackDice)
        {
            gameProgress = GameProgress.PlayerWhite;
            playerWhile.DisabledFirstDice();
            playerBlack.DisabledFirstDice();
        }
        else if (firstThrowResultWhileDice < firstThrowResultBlackDice)
        {
            gameProgress = GameProgress.PlayerBlack;
            playerWhile.DisabledFirstDice();
            playerBlack.DisabledFirstDice();
        }
        else
        {
            firstThrowResultWhileDice = 0;
            firstThrowResultBlackDice = 0;
        }
        GameProcess();
    }

    public void MoveToTheNextPlayer()
    {
        playerWhile.DisableHandControl();
        playerBlack.DisableHandControl();
        switch (gameProgress)
        {
            case GameProgress.PlayerWhite:
                gameProgress = GameProgress.PlayerBlack;
                break;

            case GameProgress.PlayerBlack:
                gameProgress = GameProgress.PlayerWhite;
                break;
        }
        GameProcess();
    }

    private void GameProcess()
    {
        Player player;
        switch (gameProgress)
        {
            case GameProgress.FirstThrow:
                FirstThrow();
                break;
            case GameProgress.PlayerWhite:
                player = playerWhile;
                player.PrepareADieBeforeThrow();
                break;
            case GameProgress.PlayerBlack:
                player = playerBlack;
                player.PrepareADieBeforeThrow();
                break;
        }
        
    }

    private void ReceiveData(NetworkDataType type, NetworkData data)
    {
        switch (type)
        {
            case NetworkDataType.FirstThrowResult:
                switch (data.PlayerColor)
                {
                    case Player.PlayerColor.While:
                        firstThrowResultWhileDice = data.ThrowDiceResult;
                        break;

                    case Player.PlayerColor.Black:
                        firstThrowResultBlackDice = data.ThrowDiceResult;
                        break;
                }
                if (firstThrowResultWhileDice != 0 && firstThrowResultBlackDice != 0)
                {
                    CountingTheResultsOfTheFirstThrow();
                }
                break;
        }
    }

}