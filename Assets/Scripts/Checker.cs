﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Checker : MonoBehaviour
{
    public enum ColorChecker
    {
        whiteChecker,
        blackChecker
    }

    public ColorChecker colorChecker;
    public float speedMove;
    public Cell myCell;
    public Cell myNewCell;
    private IEnumerator _movementToPointIEnumerator;
    public Player player;
    public List<Cell> availableCell = new List<Cell>();
    public Home home;

    private Transform _myTransform;
    private Vector3 _syncPos;
    private string _objectId;
    private float _lag;
    private bool _inHome;

    private void OnEnable()
    {
        if(_myTransform == null) { _myTransform = transform; }
        NetworkController.Instance.GetNetwork().OnReceiveData += ReceiveData;
    }

    private void OnDisable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData -= ReceiveData;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Cell>())
        {
            myNewCell = other.GetComponent<Cell>();
        }
        else if (other.GetComponent<Home>())
        {
            home = other.GetComponent<Home>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Cell>() && other.GetComponent<Cell>() == myNewCell)
        {
            myNewCell = null;
        }
        else if (other.GetComponent<Home>())
        {
            home = null;
        }
    }

    private void Update()
    {
        SyncedMovement();
    }

    public Cell GetMyCell()
    {
        return myCell;
    }

    public void Took(List<Cell> cells, List<int> values)
    {
        myNewCell = null;
        _myTransform.position += Vector3.up * 0.15f;
        var data = new NetworkData();
        data.Position = _myTransform.position;
        data.ObjectID = name;
        data.CheckerValues = values.ToArray();
        data.CellIndex = cells.IndexOf(myCell);
        NetworkController.Instance.SendData(NetworkDataType.TookChecker, data);
        myCell.TookChecker(this);
        ShowAvailableCell(cells, values);
    }

    public void Movement(Vector3 touchPosition)
    {
        Vector3 newPosition = new Vector3(touchPosition.x, _myTransform.position.y, touchPosition.z);
        if (newPosition.x <= -2.9f)
        {
            newPosition.x = -2.9f;
        }
        else if (newPosition.x >= 2.9f)
        {
            newPosition.x = 2.9f;
        }
        if (newPosition.z < -1.8f)
        {
            newPosition.z = -1.8f;
        }
        else if (newPosition.z > 1.8f)
        {
            newPosition.z = 1.8f;
        }
        ChangePosition(Vector3.MoveTowards(_myTransform.position, newPosition, speedMove * Time.fixedDeltaTime));
    }

    public void MovementToPoint(Vector3 touchPosition, Cell cell)
    {
        player._handControl.enabled = false;
        if(cell != null)
        {
            myNewCell = cell;
        }
        if(_movementToPointIEnumerator != null)
        {
            StopCoroutine(_movementToPointIEnumerator);
        }
        Vector3 newPosition = new Vector3(touchPosition.x, _myTransform.position.y, touchPosition.z);
        if (newPosition.x <= -2.9f)
        {
            newPosition.x = -2.9f;
        }
        else if (newPosition.x >= 2.9f)
        {
            newPosition.x = 2.9f;
        }
        if (newPosition.z < -1.8f)
        {
            newPosition.z = -1.8f;
        }
        else if (newPosition.z > 1.8f)
        {
            newPosition.z = 1.8f;
        }
        _movementToPointIEnumerator = MovementToPointIEnumerator(newPosition);
        StartCoroutine(_movementToPointIEnumerator);
    }

    public void MovementToHome(Vector3 touchPosition, Home home)
    {
        player._handControl.enabled = false;
        if (home != null)
        {
            this.home = home;
        }
        if (_movementToPointIEnumerator != null)
        {
            StopCoroutine(_movementToPointIEnumerator);
        }
        Vector3 newPosition = new Vector3(touchPosition.x, _myTransform.position.y, touchPosition.z);
        if (newPosition.x <= -2.9f)
        {
            newPosition.x = -2.9f;
        }
        else if (newPosition.x >= 2.9f)
        {
            newPosition.x = 2.9f;
        }
        if (newPosition.z < -1.8f)
        {
            newPosition.z = -1.8f;
        }
        else if (newPosition.z > 1.8f)
        {
            newPosition.z = 1.8f;
        }
        _movementToPointIEnumerator = MovementToPointIEnumerator(newPosition);
        StartCoroutine(_movementToPointIEnumerator);
    }

    public void ChangePosition(Vector3 newPosition, bool isNeedChangeFastPos = false)
    {
        _myTransform.position = newPosition;
        var data = new NetworkData();
        data.Position = _myTransform.position;
        data.ObjectID = name;
        data.IsNeedChangeFastPosition = isNeedChangeFastPos;
        NetworkController.Instance.SendData(NetworkDataType.ChangeCheckerPosition, data);
    }

    private IEnumerator MovementToPointIEnumerator(Vector3 newPosition)
    {
        while (Vector3.Distance(_myTransform.position, newPosition) > 0.05f)
        {
            ChangePosition(Vector3.MoveTowards(_myTransform.position, newPosition, speedMove / 3 * Time.fixedDeltaTime));
            yield return null;
        }
        player._handControl.enabled = true;
        HideAvailableCell();
        player._handControl._checkerInHand = null;
        LetGo();
    }

    public void LetGo()
    {
        if (home != null)
        {
            DisableChecker();
            RemoveUsingValueGoToHome();
            Vector3 newPosition = home.ReturnLastChecker() != null 
                ? home.ReturnLastChecker().transform.position + home.transform.forward * 0.1f 
                : home.checkerPositionAtHome;
            _myTransform.position = newPosition;
            _myTransform.eulerAngles = new Vector3(90, 0, 0);
            SendDataOnGoHome();
            if (!home.CheckerEnterToHome(this))
            {
                player.ContinuationCheck();
            }
            else
            {
                SendDataOnGameOver();
                player.GameOver();
            }
        }
        else
        {
            if (myNewCell != null)
            {
                RemoveUsingValue();
                player.CheckerTakenFromTheFirstCell(myCell);
                myCell = myNewCell;
                SendDataOnSetNewCell(myCell.name);
                myNewCell = null;
            }
            Vector3 newPosition = myCell.NewCheckerPosition();
            ChangePosition(newPosition, true);
            myCell.AddNewChecker(this);
            player.ContinuationCheck();
        }
    }

    private void RemoveUsingValueGoToHome()
    {
        int value = 24 - player.cells.IndexOf(myCell);
        player.RemoveUsingValueGoToHome(value);
    }

    public bool AvailabilityCheck(List<Cell> cells, int value)
    {
        int positionCheck = cells.IndexOf(myCell) + value;        
        bool freeCell = false;;
        
        if (positionCheck <= 23)
        {
            if (cells[positionCheck].checkers.Count == 0 || cells[positionCheck].checkers.Last().colorChecker == colorChecker)
            {
                freeCell = true;
            }
        }
        else 
        {
            if (player.home.CheckEnabledCell(colorChecker))
            {
                freeCell = true;
            }
        }
        return freeCell;
    }

    private void ShowAvailableCell(List<Cell> cells, List<int> values, int index = -1)
    {
        int indexMyCell = cells.IndexOf(myCell);
        if(index != -1)
        {
            indexMyCell = index;
        }

        if(player.oneDice.value != player.twoDice.value)
        {
            if(values.Count == 1)
            {
                if (indexMyCell + values.Last() <= 23)
                {
                    Cell cell = cells[indexMyCell + values.Last()];
                    if (cell.checkers.Count == 0 || cell.checkers.Last().colorChecker == colorChecker)
                    {
                        cell.EnabledCell();
                        availableCell.Add(cell);
                    }
                }
                else
                {
                    if (player.home.CheckEnabledCell(colorChecker))
                    {
                        Home home = player.home;
                        home.EnabledCell();
                    }
                }
            }
            else if (values.Count == 3)
            {
                if (indexMyCell + values.First() <= 23)
                {
                    Cell cellOne = cells[indexMyCell + values.First()];
                    if (cellOne.checkers.Count == 0 || cellOne.checkers.Last().colorChecker == colorChecker)
                    {
                        cellOne.EnabledCell();
                        availableCell.Add(cellOne);
                    }
                }
                else
                {
                    if (player.home.CheckEnabledCell(colorChecker))
                    {
                        Home home = player.home;
                        home.EnabledCell();
                    }
                }
                if (indexMyCell + values[1] <= 23)
                {
                    Cell cellTwo = cells[indexMyCell + values[1]];
                    if (cellTwo.checkers.Count == 0 || cellTwo.checkers.Last().colorChecker == colorChecker)
                    {
                        cellTwo.EnabledCell();
                        availableCell.Add(cellTwo);
                    }
                }
                else
                {
                    if (player.home.CheckEnabledCell(colorChecker))
                    {
                        Home home = player.home;
                        home.EnabledCell();
                    }
                }
                if (availableCell.Count > 0)
                {
                    if (indexMyCell + values.Last() <= 23)
                    {
                        Cell cellThree = cells[indexMyCell + values.Last()];
                        if (cellThree.checkers.Count == 0 || cellThree.checkers.Last().colorChecker == colorChecker)
                        {
                            cellThree.EnabledCell();
                            availableCell.Add(cellThree);
                        }
                    }
                    else
                    {
                        if (player.home.CheckEnabledCell(colorChecker))
                        {
                            Home home = player.home;
                            home.EnabledCell();
                        }
                    }
                }
            }
        }
        else
        {
            if (indexMyCell + values.First() <= 23)
            {
                Cell cellOne = cells[indexMyCell + values.First()];
                if (cellOne.checkers.Count == 0 || cellOne.checkers.Last().colorChecker == colorChecker)
                {
                    cellOne.EnabledCell();
                    availableCell.Add(cellOne);

                    if (values.Count > 1)
                    {
                        if (indexMyCell + values[1] <= 23)
                        {
                            Cell cellTwo = cells[indexMyCell + values[1]];
                            if (cellTwo.checkers.Count == 0 || cellTwo.checkers.Last().colorChecker == colorChecker)
                            {
                                cellTwo.EnabledCell();
                                availableCell.Add(cellTwo);

                                if (values.Count > 2)
                                {
                                    if (indexMyCell + values[2] <= 23)
                                    {
                                        Cell cellThree = cells[indexMyCell + values[2]];
                                        if (cellThree.checkers.Count == 0 || cellThree.checkers.Last().colorChecker == colorChecker)
                                        {
                                            cellThree.EnabledCell();
                                            availableCell.Add(cellThree);

                                            if (values.Count > 3)
                                            {
                                                if (indexMyCell + values.Last() <= 23)
                                                {
                                                    Cell cellFour = cells[indexMyCell + values.Last()];
                                                    if (cellFour.checkers.Count == 0 || cellFour.checkers.Last().colorChecker == colorChecker)
                                                    {
                                                        cellFour.EnabledCell();
                                                        availableCell.Add(cellFour);
                                                    }
                                                }
                                                else
                                                {
                                                    if (player.home.CheckEnabledCell(colorChecker))
                                                    {
                                                        Home home = player.home;
                                                        home.EnabledCell();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (player.home.CheckEnabledCell(colorChecker))
                                        {
                                            Home home = player.home;
                                            home.EnabledCell();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (player.home.CheckEnabledCell(colorChecker))
                            {
                                Home home = player.home;
                                home.EnabledCell();
                            }
                        }
                    }
                }
            }
            else
            {
                if (player.home.CheckEnabledCell(colorChecker))
                {
                    Home home = player.home;
                    home.EnabledCell();
                }
            }
        }
    }
    public void HideAvailableCell()
    {
        var data = new NetworkData();
        data.ObjectID = name;
        NetworkController.Instance.SendData(NetworkDataType.UnselectChecker, data);
        HideAvaliableCellWithoutNetwork();
    }

    private void HideAvaliableCellWithoutNetwork()
    {
        for (int i = 0; i < availableCell.Count; i++)
        {
            availableCell[i].DisabledCell();
        }
        player.home.DisabledCell();
        availableCell.Clear();
    }

    private void RemoveUsingValue()
    {
        int value = player.cells.IndexOf(myNewCell) - player.cells.IndexOf(myCell);
        player.RemoveUsingValue(value);
    }

    private void DisableChecker()
    {
        GetComponent<CapsuleCollider>().enabled = false;
    }

    private void SendDataOnGameOver()
    {
        NetworkData data = new NetworkData();
        data.PlayerColor = player.playerColor;
        NetworkController.Instance.SendData(NetworkDataType.GameOver, data);
    }

    private void SendDataOnGoHome()
    {
        NetworkData data = new NetworkData();
        data.Position = _myTransform.position;
        data.Rotation = _myTransform.eulerAngles;
        data.ObjectID = name;
        data.PlayerColor = player.playerColor;
        NetworkController.Instance.SendData(NetworkDataType.GoHome, data);
    }

    private void SendDataOnSetNewCell(string cellID)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = name;
        data.CellID = cellID;
        NetworkController.Instance.SendData(NetworkDataType.AddCellToChecker, data);
    }

    private void ReceiveData(NetworkDataType type, NetworkData data)
    {
        switch (type)
        {
            case NetworkDataType.TookChecker:
                if (name != data.ObjectID) return;

                myCell.TookChecker(this);
                _myTransform.position = data.Position;
                List<int> values = new List<int>();
                for(int i = 0; i < data.CheckerValues.Length; i++)
                {
                    values.Add(data.CheckerValues[i]);
                }
                ShowAvailableCell(player.cells, values, data.CellIndex);
                break;
            case NetworkDataType.UnselectChecker:
                if (name != data.ObjectID) return;

                HideAvaliableCellWithoutNetwork();
                break;
            case NetworkDataType.ChangeCheckerPosition:
                if (name != data.ObjectID) return;

                if (data.IsNeedChangeFastPosition)
                {
                    _myTransform.position = data.Position;
                    _syncPos = data.Position;
                    _objectId = "";
                }
                else
                {
                    _syncPos = data.Position;
                    _objectId = data.ObjectID;
                    _lag = data.Lag;
                }
                break;
            case NetworkDataType.GameOver:
                if (player.playerColor != data.PlayerColor) return;

                player.GameOver();
                break;
            case NetworkDataType.GoHome:
                if (name != data.ObjectID) return;

                _inHome = true;
                _myTransform.position = data.Position;
                _myTransform.eulerAngles = data.Rotation;
                if(home == null)
                {
                    if(data.PlayerColor == Player.PlayerColor.While)
                    {
                        home = GameObject.Find("WhiteHome").GetComponent<Home>();
                    }
                    else
                    {
                        home = GameObject.Find("BlackHome").GetComponent<Home>();
                    }
                }
                home.CheckerEnterToHome(this);
                break;
            case NetworkDataType.AddCellToChecker:
                if(name != data.ObjectID) return;

                myCell = GameObject.Find(data.CellID).GetComponent<Cell>();
                myCell.AddNewChecker(this);
                break;
        }
    }

    private void SyncedMovement()
    {
        if (!NetworkController.Instance.IsPlayerMine()) return;
        if (name != _objectId) return;
        if (_myTransform.position == _syncPos) return;
        if (_inHome) return;

        _myTransform.position = Vector3.Lerp(_myTransform.position, _syncPos, 0.05f + _lag);
    }
}