﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandControl : MonoBehaviour
{
    public Checker _checkerInHand;

    public Camera gameCamera;
    private Vector3 _startTouhcPosition;
    private Vector3 _lastTouchPosition;
    private bool _playerMovedHisHand;
    private Player _player;

    private void Start()
    {
        _player = GetComponent<Player>();
    }

#if UNITY_EDITOR || UNITY_STANDALONE
    Touch fakeTouch;
    public Touch GetEmulatedTouch()
    {

        fakeTouch = new Touch();
        if (Input.GetMouseButtonDown(0))
        {
            fakeTouch.phase = TouchPhase.Began;
            fakeTouch.deltaPosition = new Vector2(0, 0);
            fakeTouch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            fakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            fakeTouch.phase = TouchPhase.Ended;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            fakeTouch.deltaPosition = newPosition - fakeTouch.position;
            fakeTouch.position = newPosition;
            fakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButton(0))
        {
            fakeTouch.phase = TouchPhase.Moved;
            Vector2 newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            fakeTouch.deltaPosition = newPosition - fakeTouch.position;
            fakeTouch.position = newPosition;
            fakeTouch.fingerId = 0;
        }
        else
        {
            fakeTouch = new Touch();
            fakeTouch.phase = TouchPhase.Canceled;
        }
        return fakeTouch;
    }
#endif

    private void Update()
    {
        Touch touch = new Touch();
        touch.phase = TouchPhase.Canceled;
#if UNITY_EDITOR || UNITY_STANDALONE
        touch = GetEmulatedTouch();
#else
        if(Input.touchCount > 0)
            touch = Input.GetTouch(0);
#endif

        if (touch.phase != TouchPhase.Canceled)
        {


            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (_checkerInHand == null)
                    {
                        _checkerInHand = TakeAChecker(touch);
                        if (_checkerInHand != null)
                        {
                            if ((int)_checkerInHand.colorChecker == (int)_player.playerColor && _player.PermissionToTakeFromTheFirstCell(_checkerInHand.myCell))
                            {
                                _startTouhcPosition = touch.position;
                                _lastTouchPosition = _startTouhcPosition;
                                _playerMovedHisHand = false;
                                _checkerInHand.Took(_player.cells, _player.values);
                            }
                            else
                            {
                                _checkerInHand = null;
                            }
                        }
                    }
                    else if (_checkerInHand)
                    {
                        GameObject cell = SelectCell(touch);
                        if (cell == null || cell.GetComponent<Cell>())
                        {
                            _checkerInHand.MovementToPoint(gameCamera.ScreenPointToRay(touch.position).direction * 5, cell != null ? cell.GetComponent<Cell>() : null);
                        }
                        else if (cell.GetComponent<Home>())
                        {
                            _checkerInHand.MovementToHome(gameCamera.ScreenPointToRay(touch.position).direction * 5, cell.GetComponent<Home>());
                        }
                    }
                    break;

                case TouchPhase.Moved:
                    if (_checkerInHand != null)
                    {
                        if (_playerMovedHisHand)
                        {
                            _checkerInHand.Movement(gameCamera.ScreenPointToRay(touch.position).direction * 5);
                        }
                        else
                        {
                            _lastTouchPosition = touch.position;
                            if (Vector2.Distance(_startTouhcPosition, _lastTouchPosition) >= 10)
                            {
                                _playerMovedHisHand = true;
                            }
                        }
                    }
                    break;

                case TouchPhase.Ended:
                    if (_checkerInHand != null && _playerMovedHisHand)
                    {
                        _checkerInHand.LetGo();
                        _checkerInHand.HideAvailableCell();
                        _checkerInHand = null;
                    }
                    break;
            }
        }
    }

    private Checker TakeAChecker(Touch touch)
    {
        Ray ray = gameCamera.ScreenPointToRay(touch.position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {

            GameObject takenObject = hit.collider.gameObject;
            if (takenObject.GetComponent<Checker>())
            {
                Checker takenChecker = takenObject.GetComponent<Checker>();
                Cell takenСheckerСell = takenChecker.GetMyCell();
                return takenСheckerСell.ReturnAnAvailableChecker();
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    private GameObject SelectCell(Touch touch)
    {
        Ray ray = gameCamera.ScreenPointToRay(touch.position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            GameObject takenObject = hit.collider.gameObject;
            if (takenObject.GetComponent<Cell>())
            {
                return takenObject.GetComponent<Cell>().gameObject;
            }
            if (takenObject.GetComponent<Home>())
            {
                return takenObject.GetComponent<Home>().gameObject;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
}