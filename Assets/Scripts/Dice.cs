﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    public enum TestValue
    {
        Random,
        Fixed
    }

    public int value;
    public float moveTime;
    private Rigidbody _rigidbody;

    public TestValue testValue;
    public int fixedValue;
    public Vector3 lastPosition;
    public Player player;
    private Material _material;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _material = GetComponent<Renderer>().material;
    }

    private void OnEnable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData += ReceiveData;
    }

    private void OnDisable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData -= ReceiveData;
    }

    public void SetStartPosition(Vector3 startPosition)
    {
        transform.eulerAngles = Vector3.zero;
        transform.position = startPosition;
        ResetValue();
    }

    public void Throw(bool _firstThrow)
    {
        StartCoroutine(ThrowIEnumerator(_firstThrow));
    }
    private IEnumerator ThrowIEnumerator(bool _firstThrow)
    {
        transform.localEulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
        float dirX = Random.Range(0, 500);
        float dirY = Random.Range(0, 500);
        float dirZ = Random.Range(0, 500);
        transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
        transform.rotation = Quaternion.identity;
        _rigidbody.AddForce(transform.up * 500);
        transform.localEulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
        _rigidbody.AddTorque(dirX, dirY, dirZ);
        yield return null;
        StartCoroutine(SetValue(_firstThrow));
    }

    private void ResetValue()
    {
        value = 0;
    }

    private IEnumerator SetValue(bool _firstThrow)
    {
        var _diceTransform = transform;
        Vector3 oldPosition = _diceTransform.position;
        Vector3 positionNow;
        
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            positionNow = _diceTransform.position;
            if (oldPosition == positionNow)
            {
                break;
            }
            else
            {
                oldPosition = _diceTransform.position;
            }
            var data = new NetworkData();
            data.Position = positionNow;
            data.Rotation = _diceTransform.localEulerAngles;
            data.PlayerColor = player.playerColor;
            data.ObjectID = name;
            NetworkController.Instance.SendData(NetworkDataType.ThrowDice, data);
        }

        if (testValue.Equals(TestValue.Fixed))
        {
            Vector3 newRotation = Vector3.zero;
            switch (fixedValue)
            {
                case (1):
                    newRotation = new Vector3(90, 0, 0);
                    break;
                case (2):
                    newRotation = new Vector3(0, 0, -90);
                    break;
                case (3):
                    newRotation = new Vector3(180, 0, 0);
                    break;
                case (4):
                    newRotation = new Vector3(0, 0, 0);
                    break;
                case (5):
                    newRotation = new Vector3(0, 0, 90);
                    break;
                case (6):
                    newRotation = new Vector3(-90, 0, 0);
                    break;
            }
            transform.localEulerAngles = newRotation;
        }
        yield return new WaitForSeconds(0.5f);

        Ray ray = new Ray(transform.position, Vector3.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            switch (hit.collider.gameObject.name)
            {
                case "Side1":
                    value = 1;
                    break;
                case "Side2":
                    value = 2;
                    break;
                case "Side3":
                    value = 3;
                    break;
                case "Side4":
                    value = 4;
                    break;
                case "Side5":
                    value = 5;
                    break;
                case "Side6":
                    value = 6;
                    break;
            }
        }

        if (_firstThrow)
        {
            player.GetResultFirstThrow(value);
        }
        else
        {
            player.GetResultThrow(value);
        }
    }

    public void Outline(float value)
    {
        if(_material == null)
        {
            _material = GetComponent<Renderer>().material;
        }
        _material.SetFloat("_Outline", value);
    }

    private void ReceiveData(NetworkDataType type, NetworkData data)
    {
        switch(type)
        {
            case NetworkDataType.ThrowDice:
                if (player.playerColor != data.PlayerColor) return;
                if (name != data.ObjectID) return;

                var _transform = transform;
                _transform.DOMove(data.Position, 0.25f).SetEase(Ease.Linear);
                _transform.DORotate(data.Rotation, 0.25f, RotateMode.Fast).SetEase(Ease.Linear);
                break;
        }
    }
}