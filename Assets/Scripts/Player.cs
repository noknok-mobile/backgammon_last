﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public enum PlayerColor
    {
        While,
        Black
    }

    public PlayerColor playerColor;
    public Button throwDiceButton;
    public Text gameStateText;
    public Vector3 startingPositionForTheFirstThrow;
    public Dice firstTrowDice;
    public Vector3 startingPositionForThrowOneDice;
    public Vector3 startingPositionForThrowTwoDice;
    public Dice oneDice;
    public Dice twoDice;
    public GameControl gameControl;
    private bool _firstThrow;
    private int _valueOneDice;
    private int _valueTwoDice;
    public List<int> values = new List<int>();
    public List<Cell> cells;
    public HandControl _handControl;
    private bool _firstMove;
    private int _availableCheckersToTakeFromTheFirstCell;
    public Home home;

    private void Start()
    {
        _handControl = GetComponent<HandControl>();
        Screen.orientation = ScreenOrientation.Landscape;
    }

    private void OnEnable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData += ReceiveData;
    }

    private void OnDisable()
    {
        NetworkController.Instance.GetNetwork().OnReceiveData -= ReceiveData;
    }

    public void PrepareADieBeforeTheFirstThrow()
    {
        firstTrowDice.SetStartPosition(startingPositionForTheFirstThrow);
        throwDiceButton.interactable = true;
        gameStateText.text = "Бросьте кубик что бы узнать кто ходит первым";
        if (playerColor == PlayerColor.While && !NetworkController.Instance.IsFirstPlayerSet())
        {
            throwDiceButton.interactable = false;
            gameStateText.text = "";
            return;
        }
        if (playerColor == PlayerColor.Black && NetworkController.Instance.IsFirstPlayerSet())
        {
            throwDiceButton.interactable = false;
            gameStateText.text = "";
            return;
        }
        _firstThrow = true;
    }

    public void PrepareADieBeforeThrow()
    {
        _valueOneDice = 0;
        _valueTwoDice = 0;
        oneDice.gameObject.SetActive(true);
        twoDice.gameObject.SetActive(true);
        oneDice.SetStartPosition(startingPositionForThrowOneDice);
        twoDice.SetStartPosition(startingPositionForThrowTwoDice);
        oneDice.Outline(0);
        twoDice.Outline(0);
        if (playerColor == PlayerColor.While && !NetworkController.Instance.IsFirstPlayerSet()) return;
        if (playerColor == PlayerColor.Black && NetworkController.Instance.IsFirstPlayerSet()) return;

        throwDiceButton.interactable = true;
        gameStateText.text = "Бросьте кубик что бы пойти";
    }

    public void Throw()
    {
        if (!NetworkController.Instance.IsPlayerCanThrow()) return;
        if (playerColor == PlayerColor.While && !NetworkController.Instance.IsFirstPlayerSet()) return;
        if (playerColor == PlayerColor.Black && NetworkController.Instance.IsFirstPlayerSet()) return;
        throwDiceButton.interactable = false;
        gameStateText.text = "";
        if (_firstThrow)
        {
            firstTrowDice.Throw(_firstThrow);
        }
        else
        {
            oneDice.Throw(_firstThrow);
            twoDice.Throw(_firstThrow);
        }
    }

    public void GetResultFirstThrow(int value)
    {
        _firstThrow = false;
        _firstMove = true;
        gameControl.GetResultFirstThrow(playerColor, value);
    }

    public void GetResultThrow(int value)
    {
        OnOutlineInDices();
        var data = new NetworkData();
        data.ThrowDiceResult = value;
        NetworkController.Instance.SendData(NetworkDataType.ThrowResult, data);
        if (_valueOneDice == 0)
        {
            _valueOneDice = value;
        }
        else
        {
            _valueTwoDice = value;
            GetValuesForAMove();
            if (_firstMove)
            {
                _firstMove = false;
                if(_valueOneDice == _valueTwoDice && (_valueOneDice == 3 || _valueOneDice == 4 || _valueOneDice == 6))
                {
                    _availableCheckersToTakeFromTheFirstCell = 2;
                }
                else
                {
                    _availableCheckersToTakeFromTheFirstCell = 1;
                }
            }
            else
            {
                _availableCheckersToTakeFromTheFirstCell = 1;
            }
            AvailabilityCheck();
        }
    }

    public void DisabledFirstDice()
    {
        firstTrowDice.gameObject.SetActive(false);
    }

    private void GetValuesForAMove()
    {
        if (_valueOneDice == _valueTwoDice)
        {
            for (int i = 1; i <= 4; i++)
            {
                values.Add(_valueOneDice * i);
            }
        }
        else
        {
            values.Add(_valueOneDice);
            values.Add(_valueTwoDice);
            values.Add(_valueOneDice + _valueTwoDice);
        }
    }

    private void AvailabilityCheck()
    {
        bool thereIsAnAccessibleMove = false;
        foreach (var cell in cells)
        {
            if (cell != cells.First() || _availableCheckersToTakeFromTheFirstCell > 0)
            {
                Checker checker = cell.TakeAnAvailableCheckerForVerification();
                if (checker != null && (int)checker.colorChecker == (int)playerColor)
                {
                    if (_valueOneDice == _valueTwoDice)
                    {
                        thereIsAnAccessibleMove = checker.AvailabilityCheck(cells, values.First());
                        if (thereIsAnAccessibleMove)
                        {
                            break;
                        }
                    }
                    else
                    {
                        thereIsAnAccessibleMove = checker.AvailabilityCheck(cells, values.First());
                        if (thereIsAnAccessibleMove)
                        {
                            break;
                        }
                        else if (!thereIsAnAccessibleMove && values.Count == 3)
                        {
                            thereIsAnAccessibleMove = checker.AvailabilityCheck(cells, values[1]);
                            if (thereIsAnAccessibleMove)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (thereIsAnAccessibleMove)
        {
            _handControl.enabled = true;
        }
        else
        {
            StartCoroutine(NoMovesAvailable());
        }
    }

    private IEnumerator NoMovesAvailable()
    {
        gameStateText.text = "Нет доступных ходов";
        yield return new WaitForSeconds(2);
        gameStateText.text = "";
        _handControl.enabled = false;
        values.Clear();
        SendDataOnChangePlayer();
        gameControl.MoveToTheNextPlayer();
    }

    public void RemoveUsingValue(int value)
    {
        if(_valueOneDice != _valueTwoDice)
        {
            if(values.Count == 1)
            {
                if(oneDice.value == value)
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    oneDice.Outline(0);
                }
                else if (twoDice.value == value)
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                }
                values.Remove(values.Last());
            }
            if (values.Count == 3)
            {
                if (value == values.Last())
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
                else
                {
                    if (oneDice.value == value)
                    {
                        SendDataOnChangeDiceOutline(oneDice.name, 0);
                        oneDice.Outline(0);
                    }
                    else if (twoDice.value == value)
                    {
                        SendDataOnChangeDiceOutline(twoDice.name, 0);
                        twoDice.Outline(0);
                    }
                    values.Remove(values.Last());
                    values.Remove(value);
                }
            }
        }
        else
        {
            if(values.Count == 1)
            {
                SendDataOnChangeDiceOutline(oneDice.name, 0);
                oneDice.Outline(0);
                values.Remove(values.Last());
            }
            else if(values.Count == 2)
            {
                if (value == values.Last())
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
                else
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                }
            }
            else if (values.Count == 3)
            {
                if (value == values.Last())
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
                else if(value == values[1])
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else
                {
                    values.Remove(values.Last());
                }
            }
            else if(values.Count == 4)
            {
                if (values.First() == value)
                {
                    values.Remove(values.Last());
                }
                else if (value == values[1])
                {
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else if (value == values[2])
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else if (values.Last() == value)
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
            }
        }
    }

    public void DisableHandControl()
    {
        _handControl.enabled = false;
        oneDice.gameObject.SetActive(false); 
        twoDice.gameObject.SetActive(false);
    }

    public bool PermissionToTakeFromTheFirstCell(Cell cell)
    {
        if(cell != cells.First() || _availableCheckersToTakeFromTheFirstCell > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CheckerTakenFromTheFirstCell(Cell cell)
    {
        if (cell == cells.First())
        {
            if(_availableCheckersToTakeFromTheFirstCell == 2)
            {
                _availableCheckersToTakeFromTheFirstCell = 1;
            }
            else
            {
                _availableCheckersToTakeFromTheFirstCell = 0;
            }
        }
    }

    public void ContinuationCheck()
    {
        if (values.Count == 0)
        {
            SendDataOnChangePlayer();
            gameControl.MoveToTheNextPlayer();
        }
        else
        {
            AvailabilityCheck();
        }
    }

    public void RemoveUsingValueGoToHome(int value)
    {
        if (_valueOneDice != _valueTwoDice)
        {
            if (values.Count == 1)
            {
                if (oneDice.value >= value)
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    oneDice.Outline(0);
                }
                else if (twoDice.value >= value)
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                }
                values.Remove(values.Last());
            }
            if (values.Count == 3)
            {
                if(value <= values.First())
                {
                    if (oneDice.value >= value)
                    {
                        SendDataOnChangeDiceOutline(oneDice.name, 0);
                        oneDice.Outline(0);
                    }
                    else if (twoDice.value >= value)
                    {
                        SendDataOnChangeDiceOutline(twoDice.name, 0);
                        twoDice.Outline(0);
                    }
                    values.Remove(values.Last());
                    values.Remove(values.First());
                }
                else if (value <= values[1])
                {
                    if (oneDice.value >= value)
                    {
                        SendDataOnChangeDiceOutline(oneDice.name, 0);
                        oneDice.Outline(0);
                    }
                    else if (twoDice.value >= value)
                    {
                        SendDataOnChangeDiceOutline(twoDice.name, 0);
                        twoDice.Outline(0);
                    }
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
            }
        }
        else
        {
            if (values.Count == 1)
            {
                SendDataOnChangeDiceOutline(oneDice.name, 0);
                oneDice.Outline(0);
                values.Remove(values.Last());
            }
            else if (values.Count == 2)
            {
                if(value <= values.First())
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                }
                else if (value <= values[1])
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
            }
            else if (values.Count == 3)
            {
                if (value <= values.First())
                {
                    values.Remove(values.Last());
                }
                else if (value <= values[1])
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else if (value <= values[2])
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
            }
            else if (values.Count == 4)
            {
                if (value <= values.First())
                {
                    values.Remove(values.Last());
                }
                else if (value <= values[1])
                {
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else if (value <= values[2])
                {
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    twoDice.Outline(0);
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                    values.Remove(values.Last());
                }
                else if (value <= values.Last())
                {
                    SendDataOnChangeDiceOutline(oneDice.name, 0);
                    SendDataOnChangeDiceOutline(twoDice.name, 0);
                    oneDice.Outline(0);
                    twoDice.Outline(0);
                    values.Clear();
                }
            }
        }
    }

    public void GameOver()
    {
        _handControl.enabled = false;
        gameStateText.text = "Вы выиграли!!!";
    }

    private void OnOutlineInDices()
    {
        oneDice.Outline(0.1f);
        twoDice.Outline(0.1f);
    }

    private void ReceiveData(NetworkDataType type, NetworkData data)
    {
        switch (type)
        {
            case NetworkDataType.ThrowResult:
                OnOutlineInDices();
                break;
            case NetworkDataType.ChangeDiceOutline:
                if(data.ObjectID == "OneDice")
                {
                    oneDice.Outline(data.OutlineDiceStrong);
                }
                else
                {
                    twoDice.Outline(data.OutlineDiceStrong);
                }
                break;
            case NetworkDataType.ChangePlayer:
                if (data.PlayerColor != playerColor) return;

                gameControl.MoveToTheNextPlayer();
                break;
        }
    }

    private void SendDataOnChangeDiceOutline(string id, float outlineStrong)
    {
        NetworkData data = new NetworkData();
        data.ObjectID = id;
        data.OutlineDiceStrong = outlineStrong;
        NetworkController.Instance.SendData(NetworkDataType.ChangeDiceOutline, data);
    }

    private void SendDataOnChangePlayer()
    {
        NetworkData data = new NetworkData();
        data.PlayerColor = playerColor;
        NetworkController.Instance.SendData(NetworkDataType.ChangePlayer, data);
    }
}