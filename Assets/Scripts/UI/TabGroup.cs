﻿using System.Collections.Generic;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
    [Header("list of Tabs")]
    public List<Tab> tabs;


    [Header("Colors")]
    public Color activeColor;
    public Color inactiveColor;

    [Header("BackgroundIMG and SIZE(Optional)")]

    [SerializeField] private Sprite activebackgroundIMG;
    [SerializeField] private Sprite inactivebackgroundIMG;
/*    [SerializeField] private Vector2 activeSize;
    [SerializeField] private Vector2 inactiveSize;*/

    [Header("FirstTab")]
    public Tab firstTab;

    public bool withTEXT;
/*    public bool withBackgound;*/

    private void Start()
    {
        ResetTabs();
        OnTabSelected(firstTab);
    }

    public void Subscribe(Tab tabButton)
    {
        if(tabs == null)
        {
            tabs = new List<Tab>();
        }

        tabs.Add(tabButton);
    }

    public void OnTabSelected(Tab tab)
    {
        ResetTabs();

        if (tab.icon != null)
            tab.icon.color = activeColor;

        if(tab.background != null)
        tab.background.color = activeColor;

/*        if (activeSize != null)
        tab.GetComponent<RectTransform>().sizeDelta = activeSize;*/

        if (withTEXT && tab.tmPro != null)
        {
            tab.tmPro.color = activeColor;
        }

/*        Debug.Log("OnTabSelected :" + tab.tabId);*/
    }

    public void SetTabActive(int tabId)
    {
        var allTabs = transform.GetComponentsInChildren<Tab>();
        foreach (Tab tab in allTabs)
        {
            if(tab.tabId != tabId)
            {
                continue;
            }
            OnTabSelected(tab);
        }
    }

    void ResetTabs()
    {
        foreach (Tab tab in tabs)
        {
            if(tab.icon != null)
            tab.icon.color = inactiveColor;

            if (tab.background != null)
            tab.background.color = inactiveColor;

/*            if(inactiveSize != Vector2.zero)
            tab.GetComponent<RectTransform>().sizeDelta = inactiveSize;*/

            if (withTEXT && tab.tmPro != null)
            {
                tab.tmPro.color = inactiveColor;
            }
        }
    }
}
