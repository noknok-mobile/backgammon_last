﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Tab : MonoBehaviour , IPointerClickHandler
{
    public TabGroup tabGroup;
    public TextMeshProUGUI tmPro;
    public Image icon;
    public int tabId; //name it from 0 to numberOfScreens - 1
    public Image background;

    void Awake()
    {
        tabGroup.Subscribe(this);
/*        background = GetComponent<Image>();*/
        tmPro = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OnTabSelected(this);
/*        Messenger.Broadcast<int>(GameEvents.ON_BOTTOM_TAB_PRESSED, tabId);*/
    }
}
