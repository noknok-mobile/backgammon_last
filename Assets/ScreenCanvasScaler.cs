﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCanvasScaler : MonoBehaviour
{

    [SerializeField] private RectTransform canvas;

    public bool width;
    public bool height;

    public RectTransform[] WidthtObjectsToExclude;
    public RectTransform[] HeightObjectsToExclude;

    // Start is called before the first frame update
    void Start()
    {
        

        var rectTransform = GetComponent<RectTransform>();
/*        rectTransform.position = Vector2.zero;*/

        if (width)
        {
            float minusHeight = 0f;
            foreach (RectTransform rect in WidthtObjectsToExclude)
            {
                minusHeight += rect.rect.height + 10f; //width of the object + little offset
            }
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvas.rect.width);
        }

        if (height)
        {
            float minusHeight = 0f;
            foreach (RectTransform rect in HeightObjectsToExclude)
            {
                minusHeight += rect.rect.height + 10f; //height of the object + little offset
            }
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvas.rect.height - minusHeight);
        }

    }

}
