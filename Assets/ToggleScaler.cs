﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScaler : MonoBehaviour
{
    [SerializeField] bool isOn;
    [SerializeField] float scaleValue = 1f;
    [SerializeField] float duration = 0.5f;
    [SerializeField] Image img;

    private TweenCallback fadeCallback;

    private void Awake()
    {
        fadeCallback += SetActive;
    }

    private void OnDestroy()
    {
        fadeCallback -= SetActive;
    }

    private void Start()
    {
        Vector3 scale = isOn == true ? Vector3.one : Vector3.zero;
        transform.localScale = scale;
        gameObject.SetActive(isOn);
    }

    public void Toggle()
    {

        switch (gameObject.activeInHierarchy)
        {
            case true:

                isOn = false;
                transform.DOScale(0, duration)
                    .OnComplete(fadeCallback);
                break;

            case false:
                isOn = true;
                transform.DOScale(scaleValue, duration)
                    .OnPlay(fadeCallback);
                break;
        }
    }

    public void SetActive()
    {
        gameObject.SetActive(isOn);
    }
}
