﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Outlined" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" { }
		_Color("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
		_Outline("Outline width", Range(0.0, 0.1)) = .005
	}

		SubShader
		{
			Pass
			{
				Tags{ "RenderType" = "Opaque" "Queue" = "Geometry" "IgnoreProjector" = "True" }
				ZWrite Off
				Cull Front
				ZTest Always
				CGPROGRAM
				#pragma vertex vert 
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct vData
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
				};

				half _Outline;
				fixed4 _OutlineColor;

				v2f vert(vData v)
				{
					v2f o;
					float4 outline = v.vertex + _Outline  * half4(v.normal,1);
					o.vertex = UnityObjectToClipPos(outline);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					return _OutlineColor;
				}
					ENDCG
			}

			Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1" "IgnoreProjector" = "True" }
			Cull Back
			ZTest Less

			CGPROGRAM
			#pragma surface surf Lambert noforwardadd

			sampler2D _MainTex;
			fixed4 _Color;
			struct Input
			{
				fixed2 uv_MainTex;
				fixed3 color : COLOR;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb  * _Color;
			}
			ENDCG
	}
Fallback "Diffuse"
}