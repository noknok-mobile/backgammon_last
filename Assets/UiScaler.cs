﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiScaler : MonoBehaviour
{

    [SerializeField] private RectTransform canvas;
    // Start is called before the first frame update
    void Start()
    {
        var rectTransform = GetComponent<RectTransform>();
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, canvas.rect.width * 4);
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvas.rect.height);

    }

}
