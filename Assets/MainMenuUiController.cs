﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUiController : MonoBehaviour
{

    [Header("Toggles")]
    [SerializeField] private Toggle localGame;
    [SerializeField] private Toggle privateGame;
    // Start is called before the first frame update


    public void OnCreateGameClick()
    {
        if (localGame.isOn)
            StartLocalGame();
        else
            CreateOnlineLobby();
    }

    private void CreateOnlineLobby()
    {
        NetworkController.Instance.Connect();
    }

    private void StartLocalGame()
    {
        Loader.Instance.LoadGameScene("GameScene", "", "");
    }
}
