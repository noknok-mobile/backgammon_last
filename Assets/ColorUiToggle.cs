﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ColorUiToggle : MonoBehaviour, IPointerClickHandler
{
    private Image img;

    [SerializeField] private Color onColor;
    [SerializeField] private Color offColor;

    [SerializeField] private bool isOn;

    private Color targetColor;

    public void OnPointerClick(PointerEventData eventData)
    {
        isOn = !isOn;
        SetTargetColor();
    }

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    private void Start()
    {
        img.color = isOn == true ? onColor : offColor; 
    }

    private void SetTargetColor()
    {
        targetColor = isOn == true ? onColor : offColor;
        StartCoroutine(ToggleColor());
    }

    IEnumerator ToggleColor()
    {
        while(img.color != targetColor)
        {
            img.color = Color.Lerp(img.color, targetColor, 0.01f);
            yield return null;
        }
    }
}
