﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
public class TweenToAndFroScaler : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] float duration = 0.5f;
    [SerializeField] float scaleValue = 1.5f;

    public void OnPointerClick(PointerEventData eventData)
    {
        var rect = GetComponent<RectTransform>();
        rect.pivot = new Vector2(0.5f, 0.5f);

        Sequence sequence = DOTween.Sequence();
        sequence.Append(transform.DOScale(scaleValue, duration/2f)).Append(transform.DOScale(1, duration/2f));


    }
}
