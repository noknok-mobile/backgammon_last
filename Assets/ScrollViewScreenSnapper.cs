﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewScreenSnapper : MonoBehaviour
{

    public RectTransform[] screens;
    [SerializeField] private RectTransform content;

    [SerializeField] private ScrollRect scrollRect;
    [Range(0, 1)]
    [SerializeField] private float lerpSpeed = 0.1f;
    [SerializeField] private TabGroup bottomTabGroup;

    Coroutine snapRoutine;

    //test
    public Vector3 localPos;
    public Vector3 tagetPos;

    private void Awake()
    {
        Messenger.AddListener<int>(GameEvents.ON_BOTTOM_TAB_PRESSED, SnapToChild);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<int>(GameEvents.ON_BOTTOM_TAB_PRESSED, SnapToChild);
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            StopSnapRoutine();
        }

        if(Input.GetMouseButtonUp(0) && RectTransformUtility.RectangleContainsScreenPoint(content, Input.mousePosition))
        {
            int childCount = content.childCount;

            float xWidth =  content.rect.width / childCount;

            int currentElement = Mathf.RoundToInt(content.localPosition.x / xWidth);
            currentElement = Mathf.Abs(currentElement);

            /*content.localPosition = scrollRect.GetSnapToPositionToBringChildIntoView(content.GetChild(currentElement) as RectTransform);*/
            SnapToChild(currentElement);

            bottomTabGroup.SetTabActive(currentElement);

/*            Debug.Log(currentElement);*/
        } 
    }

    private void StopSnapRoutine()
    {
        if(snapRoutine != null)
        {
            StopCoroutine(snapRoutine);
        }

    }

    public void SnapToChild(int childIndex)
    {
        StopSnapRoutine();
        snapRoutine = StartCoroutine(ChildSnap(childIndex));
    }

    IEnumerator ChildSnap(int childIndex)
    {
        Vector3 targetPosition = scrollRect.GetSnapToPositionToBringChildIntoView(content.GetChild(childIndex) as RectTransform);

        float difference = Mathf.Abs(Mathf.Abs(content.localPosition.x) - Mathf.Abs(targetPosition.x));
        while (difference < 0.005f)
        {
            content.localPosition = Vector3.Lerp(content.localPosition, targetPosition, lerpSpeed);

            localPos = content.localPosition;
            tagetPos = targetPosition;
            yield return null;
        }
        Debug.Log("Finished!");
        snapRoutine = null;
        content.localPosition = targetPosition;
    }
}
