﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/*[ExecuteInEditMode]*/
public class LoadingTimerUI : MonoBehaviour
{
    public Image[] AllChildImages;
    public float radius;
    public float angleStep;
    public float rotationAngle;



    public float rotationTime;

    // Start is called before the first frame update
    void Start()
    {
        AllChildImages = transform.GetComponentsInChildren<Image>();
        angleStep = (2 * Mathf.PI) / AllChildImages.Length;
       /* angleStep *= Mathf.Rad2Deg;*/

        rotationAngle = 0f;
        for (int i = 0; i < AllChildImages.Length; i++)
        {
            Vector3 positionOnTheCircle = Vector3.zero;

            positionOnTheCircle.y = Mathf.Sin(rotationAngle) * radius;
            positionOnTheCircle.x = Mathf.Cos(rotationAngle) * radius;
            rotationAngle += angleStep;

            AllChildImages[i].transform.localPosition = positionOnTheCircle;

        }

        //set starting transparency
        for (int i = 0; i < AllChildImages.Length; i++)
        {
            Color col = AllChildImages[i].color;
            col = Color.Lerp(Color.black, Color.white, (float)1 / i);
            AllChildImages[i].color = col;
        }

        StartCoroutine(WinkRoutine());
    }

    IEnumerator WinkRoutine()
    {

        while (true)
        {
            transform.Rotate(transform.forward, -angleStep * Mathf.Rad2Deg);
/*            Quaternion newRotation = Quaternion.AngleAxis(rotationAngle * Mathf.Rad2Deg, transform.forward);
            transform.rotation = newRotation;*/

            yield return new WaitForSeconds(rotationTime);
        }
    }
}
